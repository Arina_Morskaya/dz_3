import math
was, path = {}, []
maze = [['111111111111111111'], ['1x0010000000000001'], ['111011101111111101'], ['101010000100000001'],
        ['101011010101111111'], ['101000010100001001'], ['100011110101111101'], ['101110010100000001'],
        ['100000000101101111'], ['101101111100100001'], ['101001000000101001'], ['101101111111101001'],
        ['101000000000001001'], ['101011110100101001'], ['101000010111111001'], ['111011110100000001'],
        ['101000000101111111'], ['101011110101010101'], ['1000000101000000#1'], ['111111111111111111']]


def neighbour(neighbour_x, neighbour_y, x, y):
    if maze[x][0][y] == '0' and [x, y] not in was.values():
        was['neighbour_%s neighbour_%d' % (neighbour_x, neighbour_y)] = [x, y]
        way(x, y)
    elif maze[x][0][y] == '#':
        was['Finish'] = [x, y]


def way(x, y):
    if x <= len(maze):
        neighbour(x, y, x + 1, y)
    if x > 1:
        neighbour(x, y, x - 1, y)
    if y < len(maze[x][0]):
        neighbour(x, y, x, y + 1)
    if y > 1:
        neighbour(x, y, x, y - 1)


for i in range(len(maze)):
    for j in range(len(maze[i][0])):
        if maze[i][0][j] == 'x':
            was['Start'] = [i, j]
            way(i, j)
            path.append([i, j])
        if maze[i][0][j] == '0':
            path.append([i, j])


shortest_path = []
x, y = was['Finish']
while [x, y] != was['Start']:
    for i in path:
        if int(math.fabs(x - i[0])) == 1 and y == i[1] and [i[0], i[1]] not in shortest_path:
            x = i[0]
            shortest_path.append([x, y])
            path.remove([x, y])
        elif int(math.fabs(y - i[1])) == 1 and i[0] == x and [i[0], i[1]] not in shortest_path:
            y = i[1]
            shortest_path.append([x, y])
            path.remove([x, y])

        if [x + 1, y] not in path and [x - 1, y] not in path and [x, y + 1] not in path and [x, y - 1] not in path:
            if len(shortest_path) == 1:
                shortest_path.remove(shortest_path[len(shortest_path) - 1])
                x, y = was['Finish']
            else:
                x, y, = shortest_path[len(shortest_path) - 1]
                while ([x + 1, y] not in path) and ([x - 1, y] not in path) and ([x, y + 1] not in path) \
                        and ([x, y - 1] not in path) and [x, y] != [1, 1]:
                    shortest_path.remove([x, y])
                    x, y, = shortest_path[len(shortest_path) - 1]
print('shortest_path -', shortest_path)

